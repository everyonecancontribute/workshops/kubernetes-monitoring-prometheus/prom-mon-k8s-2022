# Kubernetes Monitoring with Prometheus Workshop 2022

This project provides exercises and solutions, and links resources to other projects referenced in the workshop slides.

### Exercises

- [Manifests for Python app deployment](prometheus-demo-service/)
- [Manifests for Blackbox exporter](blackbox-exporter/)
- [Manifests for podtato-head app](podtato-head/)


## Resources

- [Slides](https://docs.google.com/presentation/d/1Fx7s914OjusEb2488LaSBrbAv91Y7Kb-j-dp0pSD_i4/edit#slide=id.gef8c48fae9_0_0)
- Projects deployed during the workshop:
  - [prometheus_demo_service](https://gitlab.com/everyonecancontribute/observability/prometheus_demo_service)
  - [prometheus_python_service](https://gitlab.com/everyonecancontribute/observability/prometheus_python_service)
- Customized [my-kube-prometheus-2022](https://gitlab.com/everyonecancontribute/workshops/kubernetes-monitoring-prometheus/my-kube-prometheus-2022)

## Kubernetes Cluster Examples

### Remote: Civo Cloud with k3s

Follow the [civo CLI installation docs](https://www.civo.com/learn/kubernetes-cluster-administration-using-civo-cli) and use the following steps to create a cluster:


```shell
$ civo kubernetes create prom-mon
```

```shell
$ civo kubernetes config prom-mon --save --merge

$ kubectl config use-context prom-mon

$ kubectl get node
```

### Remote: Amazon EKS

Install the AWS CLI and eksctl. Configure an IAM role with root access into your shell environment.

Example for macOS:

```shell
$ brew install eksctl
```

```shell
$ eksctl create cluster --region=eu-central-1 --name prometheus-mon-k8s
```


### Remote: Amazon EKS in Fargate

Install the AWS CLI and eksctl. Configure an IAM role with root access into your shell environment.

Example for macOS:

```shell
$ brew install eksctl
```

```shell
$ vim fargate-profile.yml

apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: prom-mon-k8s
  region: eu-central-1

fargateProfiles:
  - name: fp-default
    selectors:
    - namespace: default
    - namespace: kube-system
    - namespace: monitoring
```

```shell
$ eksctl create cluster -f fargate-profile.yml
```

##### Troubleshooting

Fargate limits pods to the default configured namespaces, [if not configured in a profile](https://eksctl.io/usage/fargate-support/). Otherwise pods stay in pending with the Prometheus Operator.

```shell
$ eksctl get fargateprofile --cluster prom-mon-k8s --region eu-central-1 -o yaml

- name: fp-default
  podExecutionRoleARN: arn:aws:iam::087840540208:role/eksctl-prom-mon-k8s-cluste-FargatePodExecutionRole-1CL7D262Z8GEZ
  selectors:
  - namespace: default
  - namespace: kube-system
  status: ACTIVE
  subnets:
  - subnet-0d55ad4637af444e4
  - subnet-0544ef2eca6cbf562
  - subnet-0ea6b657e060b1979
```

Modify the file and add the `monitoring` namespace, and the main configuration settings. 

```shell
$ vim fargate-profile.yml

apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: prom-mon-k8s
  region: eu-central-1

fargateProfiles:
  - name: fp-default
    podExecutionRoleARN: arn:aws:iam::087840540208:role/eksctl-prom-mon-k8s-cluste-FargatePodExecutionRole-1CL7D262Z8GEZ
    selectors:
    - namespace: default
    - namespace: kube-system
    - namespace: monitoring
    status: ACTIVE
    subnets:
    - subnet-0d55ad4637af444e4
    - subnet-0544ef2eca6cbf562
    - subnet-0ea6b657e060b1979
```

Since Fargate profiles are immutable, delete it first and apply the new profile with `eksctl`. 

```shell
$ CLUSTER_NAME="prom-mon-k8s"

$ eksctl delete fargateprofile --cluster $CLUSTER_NAME --region=eu-central-1 --na
me fp-default --wait

$ eksctl create cluster 
```

### Local with kind

```
$ kind create cluster
```
