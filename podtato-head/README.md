# podtato-head

Modified copy from https://github.com/podtato-head/podtato-head/tree/main/delivery/kubectl

Uses `type: NodePort` to use portforwarding.

```shell
$ kubectl --namespace podtato-kubectl port-forward svc/podtato-head-entry 40000:9000
```

